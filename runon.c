/* numactl alternative */
/* 2013 H.Tomari */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#define __USE_GNU
#include <sched.h>
static void usage(void);
static void usage() {
	fputs("usage: runon cpu cpu cpu ... -  command...\n",stderr);
	exit(EXIT_FAILURE);
}
extern int main(int argc, char **argv) {
	int cpunum,i=1;
	cpu_set_t cset;
	if(argc<4) { usage(); }
	CPU_ZERO(&cset);
	while(i<argc && strcmp(argv[i],"-")) {
		cpunum=strtol(argv[i],NULL,10);
		CPU_SET(cpunum,&cset);
		i++;
	}
	i=i+1;
	if(i>=argc) { usage(); }
	if(sched_setaffinity(getpid(),sizeof(cset),&cset)) {
		perror("sched_setaffinity");
		goto bye;
	}
	execvp(argv[i],&argv[i]);
	perror("execvp");
bye:
	return EXIT_FAILURE;
}
