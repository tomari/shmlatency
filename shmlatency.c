/* Inter-thread latency tester */
/* 2013 H.Tomari */
/* usage: % lat -s &  */
/*        % lat -c 10 */
/* uses GCC intrinsics for synchronization and cmp-and-swap */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <fcntl.h>

static const unsigned long SHMSIZE=8192;
static const char *pagefilename="/tmp/page";
extern double dtime(void);
static void mbarrier(void);
static void usage(void);
static long long min(long long,long long);
static int fill0(int, unsigned long);
static void pingpong(unsigned long long *volatile, unsigned long long, unsigned long long, unsigned long long);
static void server(void);
static void client(unsigned long long);

#ifdef MIPSPRO
#define __sync_synchronize __synchronize
#define __sync_bool_compare_and_swap __compare_and_swap
#endif /* MIPSPRO */

static void mbarrier() {
	__sync_synchronize();
}
static void usage() {
	fputs("Usage: lat [-s|-c nreps/1E3]\n",stderr);
	exit(EXIT_FAILURE);
}
static long long min(long long x, long long y) { return (x<y)?x:y; }
static int fill0(int fd, unsigned long size) {
	static unsigned long long zbuf[1024];
	unsigned long nbytes=sizeof(zbuf);
	signed long ssize=size,wbytes;
	int res=-1;
	unsigned long i;
	for(i=0; i<sizeof(zbuf)/sizeof(unsigned long long); i++) {
		zbuf[i]=0;
	}
	while(ssize>0) {
		if((wbytes=write(fd,zbuf,min(nbytes,ssize)))==0) {
			goto err;
		}
		ssize-=wbytes;
	}
	res=0;
err:
	return res;
}
static void pingpong(unsigned long long *volatile dai, unsigned long long nreps, unsigned long long mynum, unsigned long long hisnum) {
	unsigned long long i;
	for(i=0; i<nreps; i++) {
		while(!__sync_bool_compare_and_swap(dai,hisnum,mynum));
	}
}
static void server() {
	int pagefd;
	unsigned long long *volatile page;
	unsigned long long nreps;
	double start,end,repss;
	int st=EXIT_FAILURE;
	puts("server");
	if((pagefd=open(pagefilename,
			O_CREAT|O_SYNC|O_RDWR|O_TRUNC,
			S_IRUSR|S_IWUSR))<0) {
		perror("server open");
		goto noclean;
	}
	if(fill0(pagefd,SHMSIZE)<0) {
		perror("fill0");
		close(pagefd);
		goto noclean;
	}
	if(MAP_FAILED==(page=mmap(NULL,SHMSIZE,
				  PROT_READ|PROT_WRITE,
				  MAP_SHARED,pagefd,0))) {
		perror("server mmap");
		close(pagefd);
		goto noclean;
	}
	close(pagefd);
servloop:
	page[0]=0;
	puts("server ready");
	while(!page[0]);
	/* client writes number of repeats */
	mbarrier();
	nreps=page[0];
	printf("server: client connected, nreps=%lld\n",nreps);
	page[0]=0;
	start=dtime();
	pingpong(page,nreps,0,1);
	end=dtime();
	repss=(end-start)/((double)nreps)*1E9;
	printf("server> time: %f sec; %f ns/pingpong\n",end-start,repss);
	goto servloop;
	st=EXIT_SUCCESS;
	munmap(page,SHMSIZE);
noclean:
	exit(st);
}
static void client(unsigned long long kreps) {
	int pagefd;
	unsigned long long *volatile page;
	unsigned long long nreps;
	double start,end,repss;
	int st=EXIT_FAILURE;
	if((pagefd=open(pagefilename,
			O_RDWR|O_SYNC))<0) {
		perror("client open");
		goto noclean;
	}
	if(MAP_FAILED==(page=mmap(NULL,SHMSIZE,
				  PROT_READ|PROT_WRITE,
				  MAP_SHARED,pagefd,0))) {
		perror("client mmap");
		close(pagefd);
		goto noclean;
	}
	close(pagefd);
	puts("client");
	nreps=kreps*1000LL;
	if(page[0]!=0) {
		puts("server not ready");
		goto clean1;
	}
	page[0]=nreps;
	mbarrier();
	start=dtime();
	pingpong(page,nreps,1,0);
	end=dtime();
	repss=(end-start)/((double)nreps)*1E9;
	printf("client> time: %f sec; %f ns/pingpong\n",end-start,repss);
	st=EXIT_SUCCESS;
clean1:
	munmap(page,SHMSIZE);
noclean:
	exit(st);
}
extern int main(int argc, char *argv[]) {
	unsigned long long nreps;
	if(argc<2) {
		usage();
	} else if(!strcmp("-s",argv[1])) {
		server();
	} else if(argc>2 && !strcmp("-c",argv[1])) {
		nreps=strtoll(argv[2],NULL,10);
		client(nreps);
	} else {
		usage();
	}
	return EXIT_FAILURE;
}
